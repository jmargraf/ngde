import torch
import torch.nn.functional as F
from torch_geometric.nn import GraphConv
from torch.nn import Embedding, Linear, Sequential, ReLU
from torch_geometric.data import HeteroData

import numpy as np
from rdkit import Chem
from rdkit.Geometry import Point3D
from rdkit.Chem import rdDepictor
from rdkit.Chem import AllChem

from ase.data import covalent_radii

import os
path = os.path.abspath(__file__)
dir_path = os.path.dirname(path)
default_model = dir_path+"/ngde_h128_inter4_sym.pt"

class nGDE(torch.nn.Module):
    def __init__(self,
                 hidden_channels: int = 16,
                 interactions: int = 2):
        super().__init__()
        self.interactions = interactions
        self.hidden_channels = hidden_channels
        self.embedding = Embedding(100, self.hidden_channels, padding_idx=0)
        self.conv1     = GraphConv(self.hidden_channels, self.hidden_channels)

        self.mlp1      = Sequential(
                                         Linear(self.hidden_channels,self.hidden_channels),
                                         ReLU(),
                                         Linear(self.hidden_channels,self.hidden_channels)
                                       )
        self.mlp2      = Sequential(
                                         Linear(2*self.hidden_channels+1,self.hidden_channels),
                                         ReLU(),
                                         Linear(self.hidden_channels,1)
                                       )
    def forward(self, data):
        x, edge_index = data['atom'].x, data['atom','bonds','atom'].edge_index
        x = self.embedding(x).squeeze(1)
        for T in range(self.interactions):
            x = self.conv1(x, edge_index)
            x = F.relu(x)
        x = self.mlp1(x)

        row, col  = data['atom','distance','atom'].edge_index
        edge_attr = data['atom','distance','atom'].edge_attr.unsqueeze(1)/15. # Factor 1/15 here is for approximate standardization of outputs
        new_edge_attr = 0.5*(self.mlp2(torch.cat([x[row], x[col], edge_attr], dim=-1)).squeeze(1)+
                             self.mlp2(torch.cat([x[col], x[row], edge_attr], dim=-1)).squeeze(1))
        return new_edge_attr*15. # Factor 15 here is for approximate standardization of outputs


def load_model(PATH=default_model):
    model = nGDE(hidden_channels=128,interactions=4)
    model.load_state_dict(torch.load(PATH))
    model.eval()
    return model

def ReadString(string,StringType='SMILES'):
    if StringType=='SMILES':
        m_raw = Chem.MolFromSmiles(string)
    elif StringType=='Sequence':
        m_raw = Chem.MolFromSequence(string)
    else:
        print("!! Unknown StringType")
        exit()
    m = Chem.AddHs(m_raw)
    qTotal = Chem.GetFormalCharge(m)
    Z = []
    i = []
    j = []
    d = []
    for atom in m.GetAtoms():
        Z.append(atom.GetAtomicNum())
    for bond in m.GetBonds():
        i.append(bond.GetBeginAtomIdx())
        j.append(bond.GetEndAtomIdx())
        d.append(covalent_radii[Z[i[-1]]]+covalent_radii[Z[j[-1]]])
        j.append(bond.GetBeginAtomIdx())
        i.append(bond.GetEndAtomIdx())
        d.append(covalent_radii[Z[i[-1]]]+covalent_radii[Z[j[-1]]])
    return i,j,Z,d,m,qTotal

def ShortestDistance(i,j,d,Z,scale_cov=1.3):
    from scipy.sparse.csgraph import dijkstra
    nV = len(Z)
    A = np.zeros((nV,nV))
    ig = []
    jg = []

    for count,Ati in enumerate(i):
        Atj = j[count]
        bondcut = scale_cov*(covalent_radii[Z[Ati]]+covalent_radii[Z[Atj]])
        if d[count] < bondcut:
            A[Ati,Atj] += d[count] #1
            ig.append(Ati)
            ig.append(Atj)
            jg.append(Ati)
            jg.append(Atj)
    D = dijkstra(csgraph=A, directed=False)
    ig_all  = []
    jg_all  = []
    D_all   = []
    for Ati in range(len(Z)):
        for Atj in range(len(Z)):
            if Ati == Atj:
                continue
            else:
                ig_all.append(Ati)
                jg_all.append(Atj)
                D_all.append(D[Ati,Atj])
    return ig,jg,D,ig_all,jg_all,D_all


def SMILEStoGraph(smiles):
    i,j,Z,d,mol,qTotal = ReadString(smiles,StringType='SMILES')

    graph = HeteroData()

    ig,jg,D,ig_all,jg_all,D_all = ShortestDistance(i,j,d,Z,scale_cov=1.3)
    edge_index_bond = torch.tensor([ig,jg], dtype=torch.long)
    edge_index_all = torch.tensor([ig_all,jg_all], dtype=torch.long)
    node_features  = torch.tensor([[z] for z in Z], dtype=torch.long)
    graph['atom'].x = node_features
    graph['atom','bonds','atom'].edge_index = edge_index_bond
    graph['atom','distance','atom'].edge_index = edge_index_all
    graph['atom','distance','atom'].edge_attr  = torch.tensor(D_all, dtype=torch.float)

    return graph,ig_all,jg_all,len(Z)

def NeuralDistance(smiles):
    model = load_model()
    graph,ig_all,jg_all,dim = SMILEStoGraph(smiles)
    dij = model(graph)
    D = np.zeros((dim,dim))
    for ind,i in enumerate(ig_all):
        D[i,jg_all[ind]] = dij[ind]
    return D

if __name__ == "__main__":
    print(NeuralDistance("c1ccccc1"))


