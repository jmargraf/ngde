# Neural Graph Distance Embedding

## Description
This repo contains a reference implementation for the nGDE method described here: [link to preprint]. 

## Dependencies
Core dependencies are [numpy](https://numpy.org/), [scipy](https://scipy.org/), [ase](https://wiki.fysik.dtu.dk/ase/index.html), [PyTorch](https://pytorch.org/), [PyG (PyTorch Geometric)](https://pytorch-geometric.readthedocs.io/en/latest/) and [rdkit](https://www.rdkit.org/).

For refinement of geometries with GFN-FF the [xtb code](https://xtb-docs.readthedocs.io/en/latest/index.html) and [python bindings](https://xtb-python.readthedocs.io/en/latest/) are additionally required.

## Usage
To generate 10 conformers for a SMILES string:

```
smiles = 'CC(=O)OCCC(/C)=C\C[C@H](C(C)=C)CCC=C'
list_opt, list_embed, rmslist = GraphDistanceEmbedding(smiles,
                                                   MDSsteps=1000,
                                                   refine=True,
                                                   epsilon=1e-4,
                                                   gdeType='neural',
                                                   n_conformers=10)
```

`list_opt` contains the FF optimized atoms objects
`list_embed` contains the MDS embedded structures
`rmslist` contains the rms deviations among the conformers

## License
Open source under the MIT License

