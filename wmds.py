#Weighted Multidimensional Scaling
import numpy as np

def RunWMDS(weights, distances, dimensionality = 3, max_iter = 1000,epsilon=1e-3):
    N_nodes = weights.shape[0]

    X = EmbedWMDS(N_nodes = N_nodes,
                    dimensionality = dimensionality,
                    weights = weights,
                    distances = distances,
                    max_iter = max_iter,
                    epsilon = epsilon,
                    verbose = False
                )
    return X

def DistanceMatrix(X,Y):
    #No loops algorithm from https://jaykmody.com/blog/distance-matrices-with-numpy/
    #Gives nan for diagonal elements in symmetric comparison sometimes, hence the np.abs()
    x2 = np.sum(X**2, axis=1) # shape of (m)
    y2 = np.sum(Y**2, axis=1) # shape of (n)

    xy = np.matmul(X, Y.T)

    x2 = x2.reshape(-1, 1)
    dists = np.sqrt(np.abs(x2 - 2*xy + y2))
    return dists

def EmbedWMDS(N_nodes = 0,
                dimensionality = 3,
                weights = np.array([0.]),
                distances = np.array([0.]),
                max_iter = 1000,
                epsilon = 1e-3,
                verbose = False
             ):

    # initialize X
    X = np.random.rand(N_nodes,dimensionality)*np.max(np.abs(distances))
    V = CalculateV(weights)
    Vplus = np.linalg.pinv(V)
    stress_old = 1e8

    for istep in range(max_iter):
        Z = X.copy()
        B = GuttmanTransform(weights, distances, Z)
        X = np.linalg.multi_dot([Vplus,B,Z])
        #X = np.linalg.solve(V, np.matmul(B, Z))
        stress = CalculateStress(weights, distances, X)
        if verbose:
            print(f"{istep} {stress:.2f} {stress/N_nodes:.2f}")
        if (np.abs(stress/N_nodes-stress_old/N_nodes) < epsilon):
            if verbose:
                print('SMACOF iterations converged!')
            break
        else:
            stress_old = stress
    return X

def GuttmanTransform(weights,distances,Z):
    B = weights*distances
    dZ = DistanceMatrix(Z,Z)
    B[dZ>0.0]  /= dZ[dZ>0.0]
    B[dZ==0.0]  = 0.0
    np.fill_diagonal(B, 0)
    Bdiag = -np.sum(B,axis=-1)
    np.fill_diagonal(B, Bdiag)
    return B

def CalculateV(weights):
    V = np.zeros(weights.shape)
    for n in range(V.shape[0]):
        for m in range(n):
            V[n,n] += weights[n,m]
            V[n,m] -= weights[n,m]
            V[m,m] += weights[n,m]
            V[m,n] -= weights[n,m]
    return V

def CalculateStress(weights,distances,X):
    dX = DistanceMatrix(X,X)
    diff = dX-distances
    np.fill_diagonal(diff, 0.)
    stress = np.sum(0.5*weights*(diff)**2)
    return stress


import matplotlib.pyplot as plt

def Test():
    X_real = np.array([[0.0,0.0],
                       [1.0,0.0],
                       [1.0,1.0],
                       [0.0,1.0]])

    D = DistanceMatrix(X_real,X_real)
    weights = np.ones_like(D)
    np.fill_diagonal(weights, 0.)
    print(D)
    X_trans = RunWMDS(weights, D, dimensionality = 2, max_iter = 50, epsilon=1e-6, verbose=True)
    print(X_trans)
    print(DistanceMatrix(X_trans,X_trans))
    plt.scatter(X_real[:,0],X_real[:,1])
    plt.scatter(X_trans[:,0],X_trans[:,1])
    plt.gca().set_aspect('equal')
    plt.show()

if __name__=="__main__":
    Test()




