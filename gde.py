import numpy as np
from rdkit import Chem
from rdkit.Geometry import Point3D
from rdkit.Chem import rdDepictor
from rdkit.Chem import AllChem

from ase import Atoms
from ase.data import covalent_radii
from xtb.ase.calculator import XTB
from ase.optimize import LBFGSLineSearch
from ase.optimize.sciopt import SciPyFminBFGS, SciPyFminCG
from NeuralGDE.ngde import NeuralDistance

from wmds import RunWMDS

from time import time

np.set_printoptions(precision=4)

def GraphDistanceEmbedding(string,
                           scale_cov=1.3,
                           gamma=1.0,
                           alpha=2,
                           MDSsteps=1000,
                           refine=False,
                           xtb=False,
                           epsilon=1e-4,
                           n_conformers=1,
                           gdeType='scaled',
                           StringType="SMILES"):
    i,j,Z,d,mol,qTotal = ReadString(string,StringType=StringType)
    if gdeType=='scaled':
        D = ShortestDistance(i,j,d,Z,scale_cov=scale_cov)
        D_scaled = scale_D(D,gamma=gamma)
    elif gdeType=='neural':
        D_scaled = NeuralDistance(string)

    list_embed = []
    list_opt   = []
    for iConf in range(n_conformers):
        X_trans = weighted_MDS(D_scaled,Z,alpha=alpha,MDSsteps=MDSsteps,epsilon=epsilon)
        #X_trans = weighted_MDS(D_scaled,Z,alpha=alpha,MDSsteps=MDSsteps)
        a_embed = Atoms(Z,X_trans)
        list_embed.append(a_embed)
    list_opt,mol_opt,rmslist = ForceFieldOpt(list_embed,mol)

    if refine:
        list_opt = GFNRefine(list_opt,qTotal,xtb=xtb)
    return list_opt, list_embed, rmslist

def DistanceGeometryWrapper(string,
                           scale_cov=1.3,
                           gamma=0.5,
                           alpha=3,
                           MDSsteps=1000,
                           refine=False,
                           xtb=False,
                           epsilon=1e-4,
                           n_conformers=1,
                           StringType="SMILES"):
    i,j,Z,d,mol,qTotal = ReadString(string,StringType=StringType)
    list_opt, mol, rmslist = RunDG(mol,Z,n_conformers=n_conformers)
    if refine:
        list_opt = GFNRefine(list_opt,qTotal,xtb=xtb)
    return list_opt, rmslist

def RunDG(mol,Z,n_conformers=1):
    list_opt = []
    rmslist = []
    m = Chem.AddHs(mol)
    ps = AllChem.ETKDGv3()
    ps.enforceChirality = False
    #ps.maxAttempts=10000
    cids = AllChem.EmbedMultipleConfs(m,n_conformers,ps)
    if len(cids)!=n_conformers:
        print("!Warning")
        #cids = AllChem.EmbedMultipleConfs(m,n_conformers,ps)
    AllChem.MMFFOptimizeMoleculeConfs(m)
    AllChem.AlignMolConformers(m, RMSlist=rmslist)
    for conf in m.GetConformers():
        r_new = conf.GetPositions()
        a_opt = Atoms(Z,r_new)
        #a_opt = list_embed[0].copy()
        #a_opt.positions = r_new
        list_opt.append(a_opt)
    return list_opt, m, rmslist

def ForceFieldOpt(list_embed,mol):
    list_opt = []
    cpmol = Chem.Mol(mol)
    AllChem.Compute2DCoords(cpmol)
    conf0 = Chem.Conformer(cpmol.GetConformer(0))
    for a_embed in list_embed:
        new_atom_ps = a_embed.get_positions()
        conf = Chem.Conformer(conf0)
        conf.Set3D(True)
        for i in range(mol.GetNumAtoms()):
            x,y,z = new_atom_ps[i]
            conf.SetAtomPosition(i,Point3D(x,y,z))
        mol.AddConformer(conf,assignId=True)
    rmslist = []
    AllChem.MMFFOptimizeMoleculeConfs(mol)
    AllChem.AlignMolConformers(mol, RMSlist=rmslist)
    for conf in mol.GetConformers():
        r_new = conf.GetPositions()
        a_opt = list_embed[0].copy()
        a_opt.positions = r_new
        list_opt.append(a_opt)
    return list_opt, mol, rmslist

def GFNRefine(list_opt,qTotal,xtb=False):
    list_refine = []
    for a_opt in list_opt:
        a_refine = a_opt.copy()
        qini = np.zeros((len(a_refine)))
        qini[0] = qTotal
        a_refine.set_initial_charges(qini)
        calculator = XTB(method="GFN-FF",logfile='gfn.log')
        a_refine.calc = calculator
        qn = SciPyFminCG(atoms=a_refine,logfile='opt.log')
        #qn = LBFGSLineSearch(atoms=a_refine,logfile='opt.log')
        qn.run(fmax=0.05, steps=5000)
        if xtb:
            sp_calc = XTB(method="GFN2XTB",logfile='gfn.log')
            a_refine.calc = sp_calc
        e = a_refine.get_potential_energy()
        list_refine.append(a_refine)
    return list_refine

def ReadString(string,StringType='SMILES'):
    if StringType=='SMILES':
        m_raw = Chem.MolFromSmiles(string)
    elif StringType=='Sequence':
        m_raw = Chem.MolFromSequence(string)
    else:
        print("!! Unknown StringType")
        exit()
    m = Chem.AddHs(m_raw)
    qTotal = Chem.GetFormalCharge(m)
    Z = []
    i = []
    j = []
    d = []
    for atom in m.GetAtoms():
        Z.append(atom.GetAtomicNum())
    for bond in m.GetBonds():
        i.append(bond.GetBeginAtomIdx())
        j.append(bond.GetEndAtomIdx())
        d.append(covalent_radii[Z[i[-1]]]+covalent_radii[Z[j[-1]]])
        j.append(bond.GetBeginAtomIdx())
        i.append(bond.GetEndAtomIdx())
        d.append(covalent_radii[Z[i[-1]]]+covalent_radii[Z[j[-1]]])
    return i,j,Z,d,m,qTotal

def FixCovDist(a_embed,i,j,Z,d,scale_cov=1.3):
    bondtuples = []
    for count,Ati in enumerate(i):
        Atj = j[count]
        bondtuples.append((Ati,Atj))
        #bondcut = scale_cov*(covalent_radii[Z[Ati]]+covalent_radii[Z[Atj]])
        #if d[count] < bondcut:

    for Ati in range(len(a_embed)):
        for Atj in range(Ati):
            bondcut = scale_cov*(covalent_radii[Z[Ati]]+covalent_radii[Z[Atj]])
            if a_embed.get_distance(Ati,Atj)<bondcut:
                if (Ati,Atj) in bondtuples:
                    pass
                else:
                    a_embed.set_distance(Ati,Atj,1.1*bondcut)
            else:
                if (Ati,Atj) in bondtuples:
                    set_distance(Ati,Atj,0.9*bondcut)
    return a_embed

def GraphLaplacian(i,j,d,Z,scale_cov=1.3):
    nV = len(Z)
    #print(Z)
    A = np.zeros((nV,nV))
    for count,Ati in enumerate(i):
        Atj = j[count]
        bondcut = scale_cov*(covalent_radii[Z[Ati]]+covalent_radii[Z[Atj]])
        if d[count] < bondcut:
            A[Ati,Atj] += 1./d[count] #1
    L = np.diag(np.sum(A,axis=1))
    #print(nV,L.shape)
    L -= A
    return A,L

def ResistanceDistance(L):
    nV = L.shape[0]
    D = np.zeros_like(L)
    phi = np.ones_like(L)
    phi /= nV
    Gamma = np.linalg.inv(L+phi)
    for Ati in range(nV):
        for Atj in range(nV):
            D[Ati,Atj] = Gamma[Ati,Ati] + Gamma[Atj,Atj] - Gamma[Ati,Atj] - Gamma[Atj,Ati]
    return(D)

def ShortestDistance(i,j,d,Z,scale_cov=1.3):
    from scipy.sparse.csgraph import dijkstra
    nV = len(Z)
    A = np.zeros((nV,nV))
    for count,Ati in enumerate(i):
        Atj = j[count]
        bondcut = scale_cov*(covalent_radii[Z[Ati]]+covalent_radii[Z[Atj]])
        if d[count] < bondcut:
            A[Ati,Atj] += d[count] #1
    D = dijkstra(csgraph=A, directed=False)
    return D


def scale_D(D,gamma=0.5):
    return gamma*D+(1.-gamma)*np.sqrt(D)

def weighted_MDS(D,Z,alpha=2,MDSsteps=1000,epsilon=1e-2):
    CovD = np.zeros_like(D)
    for Ati in range(D.shape[0]):
        CovD[Ati,Ati] = 1.0
        for Atj in range(Ati):
            CovD[Ati,Atj] = 1.3*(covalent_radii[Z[Ati]]+covalent_radii[Z[Atj]])
            CovD[Atj,Ati] = 1.3*(covalent_radii[Z[Ati]]+covalent_radii[Z[Atj]])
    weights = (D/CovD+np.eye(D.shape[0]))**-alpha-np.eye(D.shape[0])
    weights[weights>1.0] = 1.0
    X_trans = RunWMDS(weights, D, dimensionality = 3, max_iter = MDSsteps,epsilon=epsilon)

    return X_trans

def Test():
    from ase.io import write
    # Test Embedding
    smiles = 'CC(C)(O1)C[C@@H](O)[C@@]1(O2)[C@@H](C)[C@@H]3CC=C4[C@]3(C2)C(=O)C[C@H]5[C@H]4CC[C@@H](C6)[C@]5(C)Cc(n7)c6nc(C[C@@]89(C))c7C[C@@H]8CC[C@@H]%10[C@@H]9C[C@@H](O)[C@@]%11(C)C%10=C[C@H](O%12)[C@]%11(O)[C@H](C)[C@]%12(O%13)[C@H](O)C[C@@]%13(C)CO' # Huge steroid

    list_opt, list_embed, rmslist = GraphDistanceEmbedding(smiles,
                                                  MDSsteps=1000,
                                                  refine=False,
                                                  epsilon=1e-4,
                                                  n_conformers=10)
    print(rmslist)
    write('test_geos.xyz',list_opt)
    # Test refinement
    smiles = 'CC(=O)OCCC(/C)=C\C[C@H](C(C)=C)CCC=C'
    list_opt, list_embed, rmslist = GraphDistanceEmbedding(smiles,
                                                  MDSsteps=1000,
                                                  refine=True,
                                                  epsilon=1e-4,
                                                  n_conformers=10)
    print(rmslist)
    write('test_refine.xyz',list_opt)

if __name__=="__main__":
    Test()
